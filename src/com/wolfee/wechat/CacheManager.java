package com.wolfee.wechat;

import java.util.Hashtable;

public class CacheManager {
	
	private static volatile CacheManager instance = null;
	private static volatile Hashtable<String,String> value = new Hashtable<String,String>();
	private static volatile Hashtable<String,Long> time = new Hashtable<String,Long>();
	
	
	private CacheManager(){
	
	}
	
	public static CacheManager getInstance(){
		if(instance == null){
			synchronized(CacheManager.class){
				if(instance == null){
					instance = new CacheManager();
				}
			}
		}
		return instance;
	}
	
	/**
	 * 
	 * @param key 
	 * @param timeMilli ��ʱʱ��
	 * @return
	 */
	public String getToken(String key,long timeMilli){
		//String key = appid + appSecret;
		if(!time.containsKey(key)){
			return "";
		}
		Long timeValue = time.get(key);
		if(timeValue == null || (System.currentTimeMillis() - timeValue ) > timeMilli || value.get(key) == null){
			return "";
		}
		
//		System.out.println(value.get(key));
		return value.get(key);
	}
	
	
	public void putValue(String key,String value){
		CacheManager.value.put(key, value);
	}
	
	public void putTime(String key,Long value){
		CacheManager.time.put(key, value);
	}
}
