package com.wolfee.wechat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class WebHelper {
	// 发送http请求，并返回json
	public JsonObject doRequest(String urlStr, String requestMethod,String postStr) {
		JsonObject result = null;
		String reply = "{}";
		StringBuilder sb = new StringBuilder();
		// String urlStr =
		// "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+APPID+"&secret="+APPSECRET;
		URL url;
		BufferedReader bufferedReader = null;
		InputStream input = null;
		HttpsURLConnection con = null;
		try {
			//创建证书管理器
			TrustManager[] tms = {new MyX509TrustManager()};
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tms, new SecureRandom());
			SSLSocketFactory sslFactory = sslContext.getSocketFactory();
			
			
			url = new URL(urlStr);
			con = (HttpsURLConnection) url.openConnection();
			con.setRequestMethod(requestMethod);
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setUseCaches(false);
			con.setSSLSocketFactory(sslFactory);
			// 发送参数
			if (postStr != null) {
				OutputStream output = con.getOutputStream();
				output.write(postStr.getBytes("UTF-8"));
				output.close();
			}
			input = con.getInputStream();
			InputStreamReader inputReader = new InputStreamReader(input,
					"UTF-8");
			bufferedReader = new BufferedReader(inputReader);
			String str = null;
			if ((str = bufferedReader.readLine()) != null) {
				sb.append(str);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				con.disconnect();
			}
		}
		reply = sb.toString();
		result = new JsonParser().parse(reply).getAsJsonObject();
		return result;
	}
	
}
