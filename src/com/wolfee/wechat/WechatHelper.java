package com.wolfee.wechat;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class WechatHelper {

	private WebHelper wh = new WebHelper();
	
	public static final String WECHAT_REDIRECT_TYPE_BASE = "snsapi_base";
	public static final String WECHAT_REDIRECT_TYPE_USERINFO  = "snsapi_userinfo";
	
	//获取所有openid
	public JsonObject getAllOpenid(String appid, String appSecret){
		
		JsonObject allOpenid = null;
		// String url =
		// "https://api.weixin.qq.com/cgi-bin/user/get?access_token="+access_token;
		String token = getTokenFromManager(appid, appSecret);

//		System.out.println(token);
		String url = "https://api.weixin.qq.com/cgi-bin/user/get?access_token="+ token;
		allOpenid = wh.doRequest(url, "GET", null);
		return allOpenid;
	}
	
	
	/**
	 * 根据openid批量获取用户信息，最多获取100条。注意json数据格式
	 * @param appid
	 * @param appSecret
	 * @param postOpenids 格式为{openid:'',lang:''};lang的值一般为zh-CN，即简体中文
	 * @return
	 */
	public JsonObject getUserinfo(String appid, String appSecret,JsonObject...postOpenids){
		JsonObject userData = null;
		String token = getTokenFromManager(appid, appSecret);
		String url = null;
		String requestMethod = null;
		if(postOpenids != null && postOpenids.length > 0){
			if(postOpenids.length == 1){
				String openid = postOpenids[0].get("openid").getAsString();
				String lang = postOpenids[0].get("lang").getAsString();
				url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token="+token+"&openid="+openid+"&lang="+lang;
				requestMethod = "GET";
				userData = wh.doRequest(url, requestMethod,null);
			}else if(postOpenids.length <= 100){
				url = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=" + token;
				requestMethod = "POST";
				JsonArray ids = new JsonArray();
				for(JsonObject json:postOpenids){
					ids.add(json);
				}
				JsonObject postData = new JsonObject();
				postData.add("user_list", ids);
				userData = wh.doRequest(url, requestMethod,postData.toString());
			}else{
				try {
					throw new Exception("json数组数量非法，最少访问1条，最多访问100条");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return userData;
	}
	
	
	public  String getTokenFromManager(String appid, String appSecret) {
		CacheManager manager = CacheManager.getInstance();
		String key = appid + appSecret;
		String token = manager.getToken(key, 110 * 60 * 1000);
		if (token == null || "".equals(token)) {
			token = getAccessToken(appid, appSecret);
			if (token != null && !"".equals(token)) {
				manager.putTime(key, System.currentTimeMillis());
				manager.putValue(key, token);
			}
		}
//		System.out.println(token);
		return token;
	}

	private  String getAccessToken(String appid, String appSecret) {
		String result = null;
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
				+ appid + "&secret=" + appSecret;
		result = wh.doRequest(url, "GET", null).get("access_token").getAsString();
		return result;
	}
	
	
	
	
	/**
	 * 发送告警消息，返回发送结果
	 * @param appid
	 * @param appSecret
	 * @param templateId
	 * @param message
	 * @param openid
	 * @return
	 */
	
	/*public JsonArray sendAlarmMessage(String appid, String appSecret,
			String templateId, WechatAlarmMessage message, String... openid) {
		// JSONObject result = null;
		JsonArray result = new JsonArray();
		JsonObject postData = new JsonObject();
		// postData.put("touser",openid);
		postData.addProperty("template_id", templateId);
		postData.addProperty("url", "");

		JsonObject dataJSON = new JsonObject();
		JsonObject first = new JsonObject();
		first.addProperty("value", message.getFirst());
		first.addProperty("color", "#FF0000");
		dataJSON.add("first", first);

		JsonObject keynote1 = new JsonObject();
		keynote1.addProperty("value", message.getContent());
		keynote1.addProperty("color", "#000000");
		dataJSON.add("content", keynote1);

		JsonObject keynote2 = new JsonObject();
		Date d = message.getOccurtime();
		String time = "";
		if(d != null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			time = sdf.format(d);
		}
		keynote2.addProperty("value",time);
		keynote2.addProperty("color", "#000000");
		dataJSON.add("occurtime", keynote2);

		JsonObject remark = new JsonObject();
		remark.addProperty("value", message.getRemark());
		remark.addProperty("color", "#000000");
		dataJSON.add("remark", remark);

		postData.add("data", dataJSON);
//		System.out.println(postData.toString());
		// result = doRequest(SEND_MESSAGE_URL, "POST", postData.toString());
		if (openid != null && openid.length > 0) {
			JsonObject sendResult;
			String token = getTokenFromManager(appid, appSecret);
			String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="
					+ token;
			for (int m = 0; m < openid.length; m++) {
				if (openid[m] != null && !"".equals(openid[m])) {
					postData.addProperty("touser", openid[m]);
					sendResult = wh.doRequest(url, "POST",
							postData.toString());
					result.add(sendResult);
				}
			}
		}
		
		if(result != null && result.size() > 0){
			Iterator it = result.iterator();
			while(it.hasNext()){
				System.out.println(it);
			}
		}
		
		return result;
	}
	*/
	
	/**
	 * 发送模板消息，返回发送结果
	 * @param appid
	 * @param appSecret
	 * @param templateId 模板参数，具体格式参照手册的模板消息发送参数的data部分
	 * @param data 包含content和url两个key,content为必选
	 * @param openid
	 * @return
	 */
	public JsonArray sendGeneralMessage(String appid, String appSecret,
			String templateId, JsonObject data, String... openid) {
		// JSONObject result = null;
		JsonArray result = new JsonArray();
		JsonObject postData = new JsonObject();
		// postData.put("touser",openid);
		postData.addProperty("template_id", templateId);
		if(data.has("url")){
			String url = data.get("url").getAsString();
			postData.addProperty("url", url == null ? "" : url);
		}
		
		
		postData.add("data", data.get("content"));
		//System.out.println(postData.toString());
		// result = doRequest(SEND_MESSAGE_URL, "POST", postData.toString());
		if (openid != null && openid.length > 0) {
			JsonObject sendResult;
			String token = getTokenFromManager(appid, appSecret);
			String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="
					+ token;
			for (int m = 0; m < openid.length; m++) {
				if (openid[m] != null && !"".equals(openid[m])) {
					postData.addProperty("touser", openid[m]);
					sendResult = wh.doRequest(url, "POST",postData.toString());
					result.add(sendResult);
				}
			}
		}
		return result;
	}
	
	/**
	 * 获取当前公众号模板list
	 * @param appid
	 * @param appSecret
	 * @return
	 */
	public  JsonObject getTempList(String appid,String appSecret){
		JsonObject result = null;
		if(!StringUtils.isEmpty(appid) && !StringUtils.isEmpty(appSecret)){
			String token = getTokenFromManager(appid, appSecret);
			String url = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token="+token;
			result = wh.doRequest(url, "GET", null);
		}
		return result;
	}
	
	
	/**
	 * 根据shortid获取某个特定模板的id
	 * @param appid
	 * @param appSecret
	 * @param data 格式为：{
           "template_id_short":"TM00015"
       }
	 * @return 返回json格式： {
           "errcode":0,
           "errmsg":"ok",
           "template_id":"Doclyl5uP7Aciu-qZ7mJNPtWkbkYnWBWVja26EGbNyk"
       }
	 */
	public JsonObject getTemplateId(String appid,String appSecret,JsonObject data){
		JsonObject result = null;
		if(!StringUtils.isEmpty(appid) && !StringUtils.isEmpty(appSecret) && data != null && data.size() > 0){
			String token = getTokenFromManager(appid, appSecret);
			String url = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token="+token;
			result = wh.doRequest(url, "POST", data.toString());
		}
		return result;
	}
	
	
	/**
	 * 菜单设置，data的格式请参照
	 * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141013&token=&lang=zh_CN
	 * @param data
	 * @return
	 */
	public JsonObject menuSetting(String appid,String appSecret,JsonObject data){
		JsonObject result = null;
		if(data != null){
			String token = getTokenFromManager(appid, appSecret);
			String url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+token;
			result = wh.doRequest(url, "POST", data.toString());
		}
		return result;
	}
	
	/**
	 * 获取素材列表
	 * @param appid
	 * @param appSecret
	 * @param data
	 * @return
	 */
	public JsonObject getAssets(String appid,String appSecret,JsonObject data){
		JsonObject result = null;
		if(data != null){
			String token = getTokenFromManager(appid, appSecret);
			String url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token="+token;
			result = wh.doRequest(url, "POST", data.toString());
		}
		return result;
	}
	
	public JsonObject getUserInfoFromAuth(String appid,String appSecret,String code){
		JsonObject result = null;
		//String token = getTokenFromManager(appid, appSecret);
		String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+appid+"&secret="+appSecret+"&code="+code+"&grant_type=authorization_code";
		result = wh.doRequest(url, "GET",null);
		return result;
	}
	
	
	/**
	 * 生成授权url
	 * @param appid 
	 * @param url 完整形式的url(如:https://baidu.com)
	 * @param type 请使用WECHAT_REDIRECT_TYPE_USERINFO 和WECHAT_REDIRECT_TYPE_BASE两个常量中的一个
	 * @return
	 */
	public String generateRedirectUrl(String appid,String url,String type){
		String result = null;
		
		if(appid != null && url != null && type != null){
			String encodedUrl = "";
			try {
				encodedUrl = URLEncoder.encode(url, "utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			result = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appid +
					"&redirect_uri="+ encodedUrl +
					"&response_type=code" +
					"&scope=" + type +
					"&state=123#wechat_redirect";
		}
		return result;
	}
	
	/*public static void main(String[] args) {
		JsonObject postData = new JsonObject();
		
		JsonObject dataJSON = new JsonObject();
		JsonObject first = new JsonObject();
		first.addProperty("value", "告警标题");
		first.addProperty("color", "#FF0000");
		dataJSON.add("first", first);

		JsonObject keynote1 = new JsonObject();
		keynote1.addProperty("value", "告警内容");
		keynote1.addProperty("color", "#000000");
		dataJSON.add("content", keynote1);

		JsonObject keynote2 = new JsonObject();
		Date d = new Date();
		String time = "";
		if(d != null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			time = sdf.format(d);
		}
		keynote2.addProperty("value",time);
		keynote2.addProperty("color", "#000000");
		dataJSON.add("occurtime", keynote2);

		JsonObject remark = new JsonObject();
		remark.addProperty("value", "告警备注");
		remark.addProperty("color", "#000000");
		dataJSON.add("remark", remark);
		
		
		
		WechatAlarmMessage message = new WechatAlarmMessage();
		message.setContent("游戏显卡坏啦，黑屏啦，对方上高地啦。");
		message.setOccurtime(new Date());
		message.setRemark("这是第一行\n这是第二行");
		sendAlarmMessage("wx907bb142582cce37", "7c46cdf6dc64b5c44a23e89e70e0b6d2", "m5nhENRH6BHfBQp4UEsi2hTkgKG-orVZ2e0V0bpQnU0", message, "od7uAsxgiu79oB0z5CGtX71HNlBA");
		
//		JsonObject data = new JsonObject();
//		data.addProperty("template_id_short", "TM00541");
//		JsonObject json = getTemplateId("wx907bb142582cce37", "7c46cdf6dc64b5c44a23e89e70e0b6d2",data);
//		System.out.println(json.toString());
		
		//construct
		
		WechatBLL wb = new WechatBLL();
		WechatMessageAlarm wma = new WechatMessageAlarm();
		wma.setContent(new WechatMessageColumn("游戏显卡坏啦，黑屏啦，对方上高地啦。"));
		wma.setOccurtime(new WechatMessageColumn("2016-01-01"));
		wma.setRemark(new WechatMessageColumn("这是第一行\n这是第二行"));
		wma.setUrl("http://wolfee.s1.natapp.cc/admin/admin_wechatTest.action");
		String content = wb.generateWechatMessageContent(wma);
		
		WechatMessage w = new WechatMessage();
		w.setContent(content);
		w.setOpenid("od7uAsxgiu79oB0z5CGtX71HNlBA");
		w.setType((byte)WechatMessageTemplate.WECHAT_TEMPLATE_ALARM.getType());
		
		//send
		MessageSender ms = new MessageSender();
		List<Integer> result = ms.sendWechatMessage(w,"wx907bb142582cce37","7c46cdf6dc64b5c44a23e89e70e0b6d2");
		System.out.println(result.get(0));
	}*/
	
}
