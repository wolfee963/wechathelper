package com.wolfee.wechat;

import org.apache.commons.lang3.StringUtils;

public class EmojiFilter {
	
	//是否包含emoji
    public static boolean containsEmoji(String source) {
        if (StringUtils.isEmpty(source)) {
            return false;
        }
        
        int len = source.length();
        
        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);
            
            if (isNotEmojiCharacter(codePoint)) {
            	return true;
            }
        }
        
        return false;
    }
    
    
    //判断是否 “不是emoji”，属于emoji返回false，不属于emoji返回true
    private static boolean isNotEmojiCharacter(char codePoint) {
        return (codePoint == 0x0) || 
                (codePoint == 0x9) ||                            
                (codePoint == 0xA) ||
                (codePoint == 0xD) ||
                ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) ||
                ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) ||
                ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
    }
    
    
    //过滤掉emoji
    public static String filterEmoji(String source) {
        
        if (!containsEmoji(source)) {
            return source;
        }
        StringBuilder buf = null;
        
        int len = source.length();
        
        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);
            
            if (isNotEmojiCharacter(codePoint)) {
                if (buf == null) {
                    buf = new StringBuilder(source.length());
                }
                buf.append(codePoint);
            }
        }
        
        if (buf == null) {
            return source;
        } else {
            if (buf.length() == len) {            
            	buf = null;
                return source;
            } else {
                return buf.toString();
            }
        }
        
    }
    
}
